/* 
 * File:   BPDC.cpp
 * Author: thanasis
 * 
 * Created on 01 July 2013, 15:17
 */

#include "BPDC.h"

BPDC::BPDC(int numSensors, int numMotors) 
{
    reservoirNum=100;
    
    inputNum=numMotors;
    
    outputNum=numSensors;
    inputWeights.set(reservoirNum+outputNum,inputNum);
    inputNodes.set(inputNum,0);
    nodes.set(outputNum+reservoirNum,1);
     Matrix error(outputNum,1);
    
    errorBuff.assign(2,error);
    desiredSens.assign(2, error);
    weights.set(outputNum+reservoirNum,outputNum+reservoirNum);
    
    for (int i=0; i<outputNum+reservoirNum; i++)
    {
        for (int j=0; j<outputNum+reservoirNum; j++)
        { 
        
        double rndNum=(rand()%400)-200 ;
        double w=(rndNum/1000);
        weights.val(i,j)=w;
        }
        
        
    }
    for (int i=outputNum; i<outputNum+reservoirNum; i++)
    {
       
        for (int j=0; j<inputNum; j++)
        { 
           
        double rndNum=(rand()%400)-200 ;
        double w=(rndNum/1000);
        inputWeights.val(i,j)=w;
            
        }
        
        
    }
     dt=1;
     heta=0.0005;
       epsillon=0.002;
}

BPDC::~BPDC() {
}

 double BPDC::squash(double node)
{
 return tanh(node);   
    
}
 
void BPDC::setDesiredOut(Matrix fut_sensor)
{
 
    
    
    for (int i=0; i<outputNum; i++)
     {  
         desiredSens[1].val(i,0)=fut_sensor.val(i,0);
     }
   
 
}

void BPDC::getState(Matrix motors)
 {      

        inputNodes=motors;
        

 }

Matrix BPDC::simulateNet()
{
    Matrix updatedNodes;
    
   
    updatedNodes=nodes*(1-dt)+((weights*nodes.map(&squash)*dt)+
                       (inputWeights*inputNodes*dt));
      
    return updatedNodes;
           

}

int BPDC::kronneckerDelta (int i, int k)
 {
     if (i==k) return 1;
     else return 0;
 }
double BPDC::squash_derivative(double node)
{
 double k=tanh(node);
    return 1.2 - k*k;
    
}

Matrix BPDC::getModelMatrix ( Matrix unused)
{
    
     
    Matrix inputW=inputWeights.rows(outputNum,outputNum+reservoirNum-1);
    Matrix inputs=inputNodes;
    Matrix ESNWeights =weights.rows(outputNum,outputNum+reservoirNum-1);
    ESNWeights=ESNWeights.columns(outputNum,outputNum+reservoirNum-1);
   
    Matrix ESNState= nodes.rows(outputNum,outputNum+reservoirNum-1);
    ESNState.map(&squash);
    
    Matrix outputWeights=weights.columns(outputNum,outputNum+reservoirNum-1);
    outputWeights=outputWeights.rows(0,outputNum-1);
    
    Matrix outNodes=nodes.rows(0,outputNum-1);
    outNodes=outNodes.map(&squash);
    
    Matrix backWeights=weights.rows(outputNum,outputNum+reservoirNum-1);
    backWeights=backWeights.columns(0,outputNum-1);
    
   
    
    Matrix internalState=inputW*inputs+ESNWeights*ESNState+backWeights*outNodes;
    
    Matrix g_prime =internalState.map(&squash_derivative);
  //Matrix RNNState = RNNActivations.map(&squash);
   
 // const Matrix& g_prime = RNNActivations.map(&squash_derivative);
        
 
  //std::cout<<(inputWeights & g_prime)<<std::endl<<std::endl;
  return ((outputWeights * (inputW & g_prime)));
  
}
 /*
Matrix BPDC::getModelMatrix ( Matrix unused)
{
    Matrix inputW=inputWeights.rows(outputNum,outputNum+reservoirNum-1);
    Matrix outputWeights=weights.columns(outputNum,outputNum+reservoirNum-1);
    outputWeights=outputWeights.rows(0,outputNum-1);
    Matrix jacobi(outputNum,inputNum);
    Matrix reservoir_der=(nodes.rows(outputNum,reservoirNum+outputNum-1)).map(&squash_derivative);
    for (int i=0; i<outputNum; i++)
        for (int j=0; j<inputNum; j++)
        {
            jacobi.val(i,j)=(outputWeights.row(i)*reservoir_der).val(0,0)*
                            inputW.column(j).elementSum();
        
        
        }
    return jacobi;
}
*/
void BPDC::updateWeights()
{
        double sum,denominator,gamma;
        Matrix weightsChange(weights.getM(),weights.getN());
    for (int i=0; i<outputNum; i++)
    {
        sum=0;
        denominator=0;
        gamma=0;
        for (int s=0; s<outputNum; s++)
        {
            sum+=((1-dt)*kronneckerDelta(i,s)+
               (weights.val(i,s)*squash_derivative(nodes.val(s,0))*dt)*
                 errorBuff[0].val(s,0));  
            denominator=denominator+(squash(nodes.val(s,0))*squash(nodes.val(s,0)));
         
        }
       
       
       gamma=sum-errorBuff[1].val(i,0);
        for (int j=0; j<outputNum+reservoirNum; j++)
        {
        
        weightsChange.val(i,j)=(heta/dt)*(squash(nodes.val(j,0))/
                (denominator+epsillon))*gamma;
       
        }
       
    }
        
        weights=weights+weightsChange;
      
}
Matrix BPDC::predict()
{
   /* double data[8]={1,1,1,1,1,1,1,1};
    double data2[4]={0.1,0.2,0.3,0.4};
    Matrix pol(4,1,data2);
    Matrix test(2,4,data);
    Matrix apotel=test.multcolwise(pol);
    std::cout<<apotel<<std::endl;*/
    
    
    errorBuff[0]=nodes.rows(0,outputNum-1)-desiredSens[0].rows(0,outputNum-1);
    
    Matrix updatedNodes=simulateNet();
    
    errorBuff[1]=updatedNodes.rows(0,outputNum-1)-desiredSens[1].rows(0,outputNum-1);
   
    //std::cout<<errorBuff[1]<<std::endl<<std::endl;
    
    updateWeights();
    desiredSens[0]=desiredSens[1];
    nodes=updatedNodes;
    
    return nodes.rows(0,outputNum-1);
}
