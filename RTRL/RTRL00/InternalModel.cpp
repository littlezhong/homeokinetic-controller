/* 
 * File:   InternalModel.cpp
 * Author: thanasis
 * 
 * Created on 25 June 2013, 19:36
 */

#include "InternalModel.h"

Matrix weights,reservoirState,outputState;

using namespace std;
InternalModel::InternalModel(int num_Sensors,int num_Motors) 
{
   
    numInputNodes=num_Sensors;
    numReservoirNodes=20;
    numOutputNodes=num_Motors;
    weights.set(numReservoirNodes+numOutputNodes,numReservoirNodes+numOutputNodes+numInputNodes);
    //for(int count1 = 0; count1 < weights.getM(); count1++) {
    //for(int count2 = 0; count2 < weights.getN(); count2++){
     
     // weights.val(count1,count2) = random_minusone_to_one(0);
       // }
        //}
     for (int i=0; i<numReservoirNodes+numOutputNodes; i++)
    {
        for (int j=0; j<numReservoirNodes+numOutputNodes+numInputNodes; j++)
        { 
        
        double rndNum=(rand()%100)-50 ;
        double w=(rndNum/1000);
        weights.val(i,j)=w;
        }
        
        
    }
    nodeState.set(weights.getM(),1);
    
    learningRate=0.01;
     //error.set(numInputNodes,1);
     Matrix piFactor(weights.getM(),weights.getN());
      desiredSens.set(weights.getM(),1);
     
    nodeSensitivity.assign(numReservoirNodes+numOutputNodes,piFactor);
}


InternalModel::~InternalModel() {
}

Matrix InternalModel::simulateNet()
{
    Matrix updatedState=weights*nodeState;
    return updatedState;
}

 double InternalModel::squash(double node)
{
 return tanh(node);   
    
}

 void InternalModel::getState(Matrix motors)
 {      
    
    nodeState.addRows(motors.getM(),0);
    
     for (unsigned int i=0; i<motors.getM(); i++ )
        {
        nodeState.val(numOutputNodes+numReservoirNodes+i,0)=motors.val(i,0);
        }
//    std::cout<< nodeState<<std::endl<<std::endl;
 }
 
 void InternalModel::setDesiredOut (Matrix fut_sensor)
 {
     
     for (unsigned int i=0; i<fut_sensor.getM(); i++)
     {  
         desiredSens.val(i,0)=fut_sensor.val(i,0);
     }
     
 }
 
 void InternalModel::updateWeights(Matrix output)
 {
      //std::cout<<desiredSens<<std::endl;
      Matrix weightChange(weights.getM(),weights.getN());
      
      // std::cout<<desiredSens<<std::endl;
    Matrix error=desiredSens-output;
   
    for (unsigned int i=0; i<error.getM(); i++) 
    {   
        weightChange+=nodeSensitivity[i]*error.val(i,0);
        
    }
    weights+=weightChange*learningRate;
    
     
 }
 
 double InternalModel::squash_derivative(double node)
{
 double k=tanh(node);
    return 1.2 - k*k;
    
}

 
 Matrix InternalModel::getModelMatrix ( Matrix input)
{
    
     Matrix inputWeights=weights.rows(numOutputNodes,numOutputNodes+numReservoirNodes);
    inputWeights=inputWeights.columns(numReservoirNodes+numOutputNodes,inputWeights.getN()-1);
     
    Matrix ESNWeights =weights.rows(numOutputNodes,weights.getM()-1);
    ESNWeights=ESNWeights.columns(numOutputNodes,numOutputNodes+numReservoirNodes-1);
   
    Matrix ESNState= nodeState.rows(numOutputNodes,numReservoirNodes+numInputNodes-1);
    
    Matrix outputWeights=weights.columns(numOutputNodes,numReservoirNodes+numOutputNodes-1);
    outputWeights=outputWeights.rows(0,numOutputNodes-1);
   
   
    
    Matrix outputDirectWeights=weights.columns(numReservoirNodes+numOutputNodes,weights.getN()-1);
    outputDirectWeights=outputDirectWeights.rows(0,numOutputNodes-1);
   
   
    Matrix selfOutWeights=weights.rows(0,numOutputNodes-1);
    selfOutWeights=selfOutWeights.columns(0,numOutputNodes-1);
    
   
    Matrix RNNActivations = inputWeights*input+ESNWeights*ESNState;
  //Matrix RNNState = RNNActivations.map(&squash);
     
  const Matrix& g_prime = RNNActivations.map(&squash_derivative);
        
 
  
  return (outputWeights * (inputWeights & g_prime)) + outputDirectWeights;
  
}
 
 int InternalModel::kronneckerDelta (int i, int k)
 {
     if (i==k) return 1;
     else return 0;
 }
 void InternalModel::getNodeSensitivity(Matrix updatedNodes,Matrix nodesValue)
 {
     Matrix internalWeights=weights.columns(0,numReservoirNodes+numOutputNodes-1);
     for (int node=0; node<numReservoirNodes+numOutputNodes; node++)
     {
         double nodeDeriv=squash_derivative(updatedNodes.val(node,0));
         for (int rowElem=0; rowElem<numReservoirNodes+numOutputNodes; rowElem++)
         {
             for (int colElem=0; colElem<weights.getN(); colElem++)
             {
                 double summation=0;
                 for (int l=0; l<numReservoirNodes+numOutputNodes; l++)
                 {
                     summation+=internalWeights.val(node,l)*nodeSensitivity[l].val(rowElem,colElem);
                 }
                 nodeSensitivity[node].val(rowElem,colElem)=nodeDeriv*
                                    (summation+(kronneckerDelta(rowElem,node)*nodesValue.val(colElem,0)));
             }
         }
     }
     
 }
 
Matrix InternalModel::predict()
{
   Matrix currentNodes=nodeState;
   Matrix prevWeights =weights;
   
   Matrix updatedNodes=simulateNet();
   //std::cout<<updatedNodes<<std::endl<<std::endl;
  getNodeSensitivity(updatedNodes,currentNodes);
  
   updateWeights(updatedNodes);
//   std::cout<<weights<<std::endl;
  //std::cout<<sensit[0]<<std::endl;
   
   nodeState=updatedNodes.map(&squash);
   return nodeState.rows(0,numOutputNodes-1);
}
