/* 
 * File:   LinearRegression.cpp
 * Author: thanasis
 * 
 * Created on 03 July 2013, 20:35
 */

#include "LinearRegression.h"

 LinearRegression::LinearRegression(int numSensors, int numMotors,int degree) 
{
     polyDegree=degree;
     inputNum=numMotors;
     outputNum=numSensors;
     inputs.set((polyDegree*inputNum)+1,1);
     inputs.val(0,0)=1;//set bias (for w0)
     weights.set(inputs.getM(),outputNum);
     error.set(inputNum,0);
     desiredOut.set(outputNum,1);
     invCorrelation.set(inputs.getM(),inputs.getM());
     invCorrelation.toId();
     invCorrelation=invCorrelation*(1/0.001);
     forgettFactor=1;
     
}

LinearRegression::~LinearRegression() {
}

void LinearRegression::getState(Matrix motors)
{
    
     if (polyDegree==1)
    {
        for (int i=1; i<inputNum+1; i++)
        {
        inputs.val(i,0)=motors.val(i-1,0);    
        
        }
    }
     else if (polyDegree==2)
     {
         int count=0;
     for (int i=1; i<inputNum*2; i=i+2)
        {
        inputs.val(i,0)=motors.val(count,0);    
        inputs.val(i+1,0)=motors.val(count,0)*motors.val(count,0);
        count++;
        }
     
     }
     
}

Matrix LinearRegression::getModelMatrix ( Matrix unused)
{
   if (polyDegree==1)
   {
        return weights.rows(1,weights.getM()-1);
    
    }
   else if (polyDegree==2)
   {
       Matrix jacobianNet(inputNum,outputNum);
       for (int i=0; i<inputNum; i++)
       {
           for (int j=0; j<outputNum;j++)
           {
           jacobianNet.val(i,j)=((weights.val(j+1,i))*2)+
                                 weights.val(j+1,i);
           
           }
       
       }
       
       return jacobianNet;
   
   }
    
}
void LinearRegression::setDesiredOut(Matrix sensors)
 {
 
    desiredOut=sensors;
    
 }
void LinearRegression::updateWeights()
 {
   
    double denominator;
    denominator=((inputs^T)*invCorrelation*inputs).val(0,0);
   
    denominator=1/(denominator+forgettFactor);
    
    invCorrelation=invCorrelation-((((invCorrelation*inputs)*(inputs^T))*invCorrelation)*
                    denominator);
   
    
    invCorrelation=invCorrelation*(1/forgettFactor);
    weights=weights+(invCorrelation*inputs*(error^T));
    
    
 }
Matrix LinearRegression::predict()
{
   
    predictedOut=(inputs^T)*weights;
    error=desiredOut-(predictedOut^T);
    updateWeights();
    return predictedOut^T;
}
