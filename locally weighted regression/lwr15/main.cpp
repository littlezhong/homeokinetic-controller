/***************************************************************************
 *   Copyright (C) 2005 by Robot Group Leipzig                             *
 *    martius@informatik.uni-leipzig.de                                    *
 *    fhesse@informatik.uni-leipzig.de                                     *
 *    der@informatik.uni-leipzig.de                                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 *
 ***************************************************************************/

#include <ode_robots/simulation.h>

#include <ode_robots/odeagent.h>
#include <ode_robots/terrainground.h>
#include <ode_robots/operators.h>
#include <ode_robots/playground.h>
#include <selforg/noisegenerator.h>
#include <selforg/one2onewiring.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <time.h>
#include <sys/stat.h>
#include <selforg/position.h>
#include "sox.h"
//#include <selforg/sox.h>
#include <ode_robots/fourwheeled.h>
//#include "hexapod.h"

// fetch all the stuff of lpzrobots into scope
using namespace lpzrobots;


FILE* file;

bool track=true;
bool energy=true;
class ThisSim : public Simulation {
public:
  
  Sox* controller;
  //  SeMoX* controller;
  //InvertMotorNStep* controller;
  OdeRobot* vehicle;
   //std::cout<<"1"<<std::endl;
    char filename[1024];
    //std::cout<<"2"<<std::endl;
     char date[128];
     //std::cout<<"3"<<std::endl;

      //std::cout<<"4"<<std::endl;
      //strftime(date, 128, "%F_%H-%M-%S", localtime(&t));
      //std::cout<<"5"<<std::endl;
   
  // starting function (executed once at the beginning of the simulation loop)
  void start(const OdeHandle& odeHandle, const OsgHandle& osgHandle, GlobalData& global)
  {
time_t t = time(0);    
strftime(date, 128, "%F_%H-%M-%S", localtime(&t));
      setCameraHomePos(Pos(-6.32561, 5.12705, 3.17278),  Pos(-130.771, -17.7744, 0));

   
    global.odeConfig.setParam("noise", 0.05);
  //  global.odeConfig.setParam("controlinterval", 2);
    global.odeConfig.setParam("cameraspeed", 250);
    global.odeConfig.setParam("gravity", -6);
    setParam("UseQMPThread", false);

      
    
    
    
    Playground* playground =
      new Playground(odeHandle, osgHandle, osg::Vec3(20, 0.2, 1), 1,false);
    //     // playground->setColor(Color(0,0,0,0.8));
    //playground->setGroundColor(Color(2,2,2,1));
    playground->setPosition(osg::Vec3(0,0,0.05));  // playground positionieren und generieren
        global.obstacles.push_back(playground); 
    
    // use Playground as boundary:
    TerrainGround* terrainground = 
      new TerrainGround(odeHandle, osgHandle.changeColor(Color(83.0/255.0,48.0/255.0,0.0/255.0)),
			"rough6.ppm","", 25, 25, 0.15/*0.15*/);
      terrainground->setPose(osg::Matrix::translate(0, 0, 0.01));
      global.obstacles.push_back(terrainground);

//    Boxpile* boxpile = new Boxpile(odeHandle, osgHandle);
//    boxpile->setColor("wall");
//    boxpile->setPose(ROTM(M_PI/5.0,0,0,1)*TRANSM(0, 0,0.2));
//    global.obstacles.push_back(boxpile);


    //     global.obstacles.push_back(playground);
    // double diam = .90;
    // OctaPlayground* playground3 = new OctaPlayground(odeHandle, osgHandle, osg::Vec3(/*Diameter*/4.0*diam, 5,/*Height*/ .3), 12,
    //                                                  false);
    // //  playground3->setColor(Color(.0,0.2,1.0,1));
    // playground3->setPosition(osg::Vec3(0,0,0)); // playground positionieren und generieren
    // global.obstacles.push_back(playground3);

    

    //    addParameter("gamma_s",&teacher);
    global.configs.push_back(this);

   
    /*******  H E X A P O D  *********/
    

    FourWheeledConf myConf        = FourWheeled::getDefaultConf();
    myConf.size=0.35;
    myConf.useBumper=true;    //    myHexapodConf.numTarsusSections = 2;
    myConf.force=5;
   // myConf.useButton=true;    
    
    OdeHandle rodeHandle = odeHandle;
   // rodeHandle.substance.toRubber(20);
    
    vehicle = new FourWheeled(rodeHandle, osgHandle.changeColor("Green"),
                          myConf, "Hexapod_");
    
//    std::cout<<vehicle->energyConsumption()<<std::endl;
    // on the top
//   vehicle->place(osg::Matrix::rotate(M_PI*1,1,0,0)*osg::Matrix::translate(0,0,1));
    // normal position
  vehicle->place(osg::Matrix::translate(0,0,0.5));

//     InvertMotorNStepConf cc = InvertMotorNStep::getDefaultConf();
//     cc.cInit=1.0;
//     cc.useS=false;
    //    cc.someInternalParams=true;
//     InvertMotorNStep *semox = new InvertMotorNStep(cc);
//     semox->setParam("steps", 1);
//     semox->setParam("continuity", 0.005);
//     semox->setParam("teacher", teacher);

    
    std::cout<<"6"<<std::endl;
     controller = new Sox(1.2, false);
   controller->setParam("epsC",0.05);
   controller->setParam("epsA",0.01);
    controller->setParam("Logarithmic",1);
    
    
    
std::cout<<"7"<<std::endl;
    One2OneWiring* wiring = new One2OneWiring(new ColorUniformNoise(0.1));
    // the feedbackwiring feeds here 75% of the motor actions as inputs and only 25% of real inputs
//     AbstractWiring* wiring = new FeedbackWiring(new ColorUniformNoise(0.1),
//                                                 FeedbackWiring::Motor, 0.75);
    //global.plotoptions.push_back(PlotOption(GuiLogger,Robot,5));
    OdeAgent* agent = new OdeAgent(global);
    agent->init(controller, vehicle, wiring);
    
    // add an operator to keep robot from falling over
    //323 001 323 003
  agent->addOperator(new LimitOrientationOperator(Axis(10,2,0), Axis(0,0,6),M_PI*(0.4), 31));
     
    global.agents.push_back(agent);
    global.configs.push_back(agent);
    
   
       sprintf(filename, "%s_LRG_%s.log",
                vehicle->getTrackableName().c_str(), date);
       
  
  }
  
  // // overloaded from configurable
  // virtual bool setParam(const paramkey& key, paramval val, bool traverseChildren){
  //   bool rv = Configurable::setParam(key,val);
  //   if(key=="gamma_s"){
 //     controller->setParam("gamma_teach", teacher);
  //   }
  //   return rv;
  // }
  
  virtual void addCallback(GlobalData& globalData, bool draw, bool pause, bool control) {
      
      file = fopen(filename, "a");
   
  //fprintf(file, " x y z en");
  
//  double en=vehicle->energyConsumption();

  double er=controller->getError();
  
  Position p = vehicle->getPosition();
  
      fprintf(file, " %g %g %g ", p.x, p.y,er);
     const matrix::Matrix& o = vehicle->getOrientation();
      for(int i=0; i<3; i++){
        for(int j=0; j<3; j++){
          fprintf(file, " %g", o.val(i,j));
        }
      }
      fprintf(file, "\n");
       fclose(file);
       //file=0;
  }
  

};


int main (int argc, char **argv)
{
  
  
  ThisSim sim;
  
  sim.setGroundTexture("sandyground.rgb");
  sim.setCaption("lpzrobots Simulator Homeokinesis -  One-Layer Controller");
  return sim.run(argc, argv) ? 0 :  1;
}



